// DOM це програмний інтерфейс який допомагає через Java Script взаємодіяти з HTML документом.
//
// Через метод innerHTML можна записати обо отримати HTML контект та його наповннення, в випадку innerText ми взаємодіємо
// тільки з текстовим наповненням не змінюючи HTML частину.
//
// Звернутися через JS до елемента HTML можна через document.querySelector або через document.getElementBy
// Всі методі дієві та дають можливість шукати елемент за CSS классом, id, html тегом, html атрибутом, ім'ям та іншими параметрами




const paragraphs =  document.querySelectorAll("p");
console.log(paragraphs);

function changeColour () {
    for (const item of paragraphs) {
        item.style.backgroundColor = '#ff0000';
    }
}
changeColour();



const searchElementOne = document.getElementById('optionsList');
console.log(searchElementOne);

const searchParentElementOne = searchElementOne.parentNode;
console.log(searchParentElementOne);

const testParagraph =  document.getElementById('testParagraph');

const testParagraphContent = testParagraph.innerHTML;

testParagraph.innerHTML = 'This is a paragraph';



const elementTwo =  document.getElementsByClassName('main-header');

const childs = elementTwo[0].childNodes;
console.log(childs);

for (child of childs){
    child.className = 'nav-item';
}
console.log(childs);

//у мене в коді немає елемента з класом section-title тому я використав клас products-list-item//

const elementsForDelete = document.querySelectorAll('.products-list-item');
console.log(elementsForDelete);

for (element of elementsForDelete){
    element.classList.remove('products-list-item');
}
console.log(elementsForDelete);

